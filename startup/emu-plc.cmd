require emu-plc,2.1.1

# s7plcConfigure (PLCname, IPaddr, port, inSize, outSize, bigEndian, recvTimeout, sendInterval (ms))
s7plcConfigure ("plc", "172.16.60.33", 2000, 40, 6, 1, 1000, 500)

# PLC signals
dbLoadRecords(emu_input.db)
dbLoadRecords(emu_output.db)

